export class ProductoModel{
    nombre: string;
    valor: number;
    marca: string;

    constructor(){
        this.nombre = '';
        this.marca = '';
        this.valor = 0;
    }
}