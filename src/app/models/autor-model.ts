export class AutorModel{
    nombre: string;
    apellidos: string;
    edad: number;
    añosExperiencia: number;

    constructor(){
        this.nombre = '';
        this.apellidos = '';
        this. edad = 0;
        this.añosExperiencia = 0;
    }
}