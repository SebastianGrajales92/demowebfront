import { AutorModel } from "./autor-model";

export class LibroModel{
    titulo: string;
    tema: string;
    precio:  number;
    autor: AutorModel;

    constructor(){
        this.titulo = '';
        this.tema = '';
        this.precio = 0;
        this.autor = null;
    }

}