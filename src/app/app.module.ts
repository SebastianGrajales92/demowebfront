import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { VistaContratoComponent } from './VistaContrato/vista-contrato/vista-contrato.component';
import {HttpClientModule} from '@angular/common/http';
import { LibrosComponent } from './VistaLibros/libros/libros.component'
import { ProductoRoutingModule } from './VistaContrato/producto-routing.module';
@NgModule({
  declarations: [
    AppComponent,
    VistaContratoComponent,
    LibrosComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ProductoRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
