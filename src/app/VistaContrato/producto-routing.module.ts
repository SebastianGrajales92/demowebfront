import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VistaContratoComponent } from './vista-contrato/vista-contrato.component';
import { LibrosComponent } from '../VistaLibros/libros/libros.component';

const routes: Routes = [
  {
    path: 'list-productos',
    component: VistaContratoComponent

  },
  {
    path: 'list-libros',
    component: LibrosComponent
  },
  {
    path: '',
    redirectTo: 'list-productos',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    [RouterModule.forRoot(routes)]
  ],
  exports: [RouterModule]
})
export class ProductoRoutingModule { }
