import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceProductoService {

  constructor(private http: HttpClient) { }

  public obtenerProductos():Observable<any>{
    return this.http.get('http://localhost:9898/api/producto/get-all');
  }
}
