import { Component, OnInit } from '@angular/core';
import { ProductoModel } from 'src/app/models/producto-model';
import { ServiceProductoService } from '../service/service-producto.service';

@Component({
  selector: 'app-vista-contrato',
  templateUrl: './vista-contrato.component.html',
  styleUrls: ['./vista-contrato.component.css']
})
export class VistaContratoComponent implements OnInit {

  lista: Array<ProductoModel>;

  constructor(
    private service:ServiceProductoService
  ) { }

  ngOnInit(): void {
    this.obtenerProductos();
  }


  private obtenerProductos(){
    this.service.obtenerProductos().subscribe(response => {
      this.lista= response;
    });
  }

  inicializar() {
  //   this.lista = [
  //     {
  //       numeroContrato: 1234,
  //       idCliente: 1234,
  //       nombre: "Pepito",
  //       tipoPlan: "Contado",
  //       fecha: "2021/03/24"
  //     },
  //     {
  //       numeroContrato: 12345,
  //       idCliente: 12345,
  //       nombre: "Juan",
  //       tipoPlan: "Credito",
  //       fecha: "2021/03/24"
  //     },
  //     {
  //       numeroContrato: 12346,
  //       idCliente: 12346,
  //       nombre: "Lucia",
  //       tipoPlan: "Contado",
  //       fecha: "2021/03/24"
  //     }
  //   ]
  }

}
