import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaContratoComponent } from './vista-contrato.component';

describe('VistaContratoComponent', () => {
  let component: VistaContratoComponent;
  let fixture: ComponentFixture<VistaContratoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VistaContratoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
