import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LibrosServiceService {

  constructor(private http:HttpClient) { }

  public obtenerLibros():Observable<any>{
    return this.http.get('http://localhost:9898/api/demo/libros/get');
  }
}
