import { Component, OnInit } from '@angular/core';
import { LibroModel } from 'src/app/models/libros-model';
import { LibrosServiceService } from '../service/libros-service.service';

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit {

  listaLibros : Array<LibroModel>;
  constructor(private service: LibrosServiceService) { }

  ngOnInit(): void {
    this.obtenerLibros();
  }

  obtenerLibros(){
    this.service.obtenerLibros().subscribe(response=>{
      this.listaLibros = response;
    })
  }

}
